﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public enum Rhythm
{
    Empty = 0,
    FourQuarters = 1,
    TwoHalfs = 2,
    OneWhole = 3
}

public class Phrase 
{
    public int length; //total length of phrase in beats
    public List<Note> pattern;
    public int owner; //index [0-2] of enemy that created note 

    public Phrase()
    {
        this.length = 4;
        this.pattern = new List<Note> { };
        this.owner = 0;
    }

    public Phrase(int length = 4, int owner = 0)
    {
        this.length = length;
        this.pattern = new List<Note> { };
        this.owner = owner;
    }

    public Phrase(Rhythm rhythmKey = Rhythm.Empty, KeyCode defKey = KeyCode.Space, int length = 4, int owner = 0)
    {
        this.length = length;
        this.owner = owner;
        switch (rhythmKey)
        {
            case Rhythm.Empty:
                this.pattern = new List<Note> {};
                break;
            case Rhythm.FourQuarters:
                this.pattern = new List<Note> { new Note(0d, defKey), new Note(1d, defKey), new Note(2d, defKey), new Note(3d, defKey), };
                break;
            case Rhythm.TwoHalfs:
                this.pattern = new List<Note> { new Note(0d, defKey), new Note(2d, defKey) };
                break;
            case Rhythm.OneWhole:
                this.pattern = new List<Note> { new Note(0d, defKey) };
                break;
            default:
                this.pattern = new List<Note> { };
                break;
        }
        //this.pattern = new List<Note> { /*Note(0f, defKey), Note(1f, defKey), Note(2f, defKey), Note(3f, defKey)*/ };
    }

    public Phrase(double[] beats, KeyCode defKey = KeyCode.Space, int length = 4, int owner = 0)
    {
        this.length = length;
        this.owner = owner;
        this.pattern = new List<Note>();
        for (int i = 0; i < beats.Length; i++)
        {
            this.pattern.Add(new Note(beats[i], defKey));
        }
    }

    public Phrase(List<Note> pattern, int length, int owner)
    {
        this.pattern = new List<Note>(pattern);
        this.length = length;
        this.owner = owner;
    }

    //copy constructor
    public Phrase(Phrase other)
    {
        if (other != null)
        {
            this.length = other.length;
            this.owner = other.owner;
            this.pattern = new List<Note>();
            foreach (Note note in other.pattern)
            {
                this.pattern.Add(new Note(note));
            }
        }
    }
}
