﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum InstrumentCode
{
    Bongo,
    PanFlute,
    Lute
}


public class Instrument
{

    public InstrumentCode instType;
    public Dictionary<KeyCode, GameObject> noteMapping;


    public Instrument()
    {
        noteMapping = new Dictionary<KeyCode, GameObject>();
    }

    public Instrument(InstrumentCode instCode)
    {

        instType = instCode;
        noteMapping = new Dictionary<KeyCode, GameObject>();

        switch (instCode)
        {
            case InstrumentCode.Bongo:
                //Load Bongo note mapping
                noteMapping.Add(KeyCode.C, Resources.Load("Note Prefabs/Note C") as GameObject);
                noteMapping.Add(KeyCode.M, Resources.Load("Note Prefabs/Note M") as GameObject);
                break;


            case InstrumentCode.PanFlute:
                //Load PanFlute note mapping
                noteMapping.Add(KeyCode.S, Resources.Load("Note Prefabs/Note S") as GameObject);
                noteMapping.Add(KeyCode.D, Resources.Load("Note Prefabs/Note D") as GameObject);
                noteMapping.Add(KeyCode.F, Resources.Load("Note Prefabs/Note F") as GameObject);
                noteMapping.Add(KeyCode.J, Resources.Load("Note Prefabs/Note J") as GameObject);
                noteMapping.Add(KeyCode.K, Resources.Load("Note Prefabs/Note J") as GameObject);
                noteMapping.Add(KeyCode.L, Resources.Load("Note Prefabs/Note J") as GameObject);

                break;
            case InstrumentCode.Lute:
                noteMapping.Add(KeyCode.Q, Resources.Load("Note Prefabs/Note Q") as GameObject);
                noteMapping.Add(KeyCode.W, Resources.Load("Note Prefabs/Note W") as GameObject);
                noteMapping.Add(KeyCode.E, Resources.Load("Note Prefabs/Note E") as GameObject);
                noteMapping.Add(KeyCode.R, Resources.Load("Note Prefabs/Note R") as GameObject);
                //noteMapping.Add(KeyCode.T, Resources.Load("Note Prefabs/Player T") as GameObject);
                break;
            default:
                //Load default (if we have one?) note mapping
                break;
        }

    }




}
