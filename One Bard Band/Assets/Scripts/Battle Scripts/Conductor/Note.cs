﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note 
{

    public double beat;
    public double length;
    public KeyCode key;
    //public bool heardOrLoaded;
    public bool isHeard;
    public bool isLoaded;


    public Note(double beat = 0d, KeyCode inputKey = KeyCode.Space, double length = 0d)
    {
        this.beat = beat;
        this.key = inputKey;
        this.length = length;
        this.isHeard = false;
        this.isLoaded = false;

    }
    //copy constructor
    public Note(Note other)
    {
        this.beat = other.beat;
        this.key = other.key;
        this.length = other.length;
        this.isHeard = other.isHeard;
        this.isLoaded = other.isLoaded;
    }

    public void SetIsHeard(bool heard)
    {
        isHeard = heard;
    }

    public void SetIsLoaded(bool loaded)
    {
        isLoaded = loaded;
    }

}
