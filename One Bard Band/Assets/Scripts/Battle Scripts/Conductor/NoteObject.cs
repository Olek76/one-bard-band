﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteObject : MonoBehaviour
{

    public float tempo;
    public float speedModifier;
    public double len; //length of note length of 0 denotes non-sustained note
    public double timeHoldStart;
    public double timeHeld;
    public bool isSustained = false;
    public bool noteHeld;
    public bool isHealing;
    public bool wasHit;

    public bool canBePressed;
    public KeyCode keyToPress;

    public float timeAlive;
    public float timeToLive;

    public Sprite normalSprite;
    public Sprite healingSprite;

    public int owner;

    public void SetHealing()
    {
        isHealing = true;
        gameObject.GetComponent<SpriteRenderer>().sprite = healingSprite;
    }

    void FixedUpdate()
    {
        transform.position -= new Vector3(tempo * speedModifier * Time.deltaTime, 0f);
        timeAlive += Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {


        //note hit
        if (Input.GetKeyDown(keyToPress) && canBePressed)
        {
            if (!isSustained)
            {
                gameObject.SetActive(false);
                wasHit = true;
                Conductor.instance.NoteHit(owner, isHealing);
                Conductor.instance.notesHit++;
            }
            else
            {
                wasHit = true;
                Conductor.instance.notesHit++;
                noteHeld = true;
                timeHoldStart = MapData.instance.songs.hearingPositionInBeats;
                GetComponentInChildren<NoteTrail>().trailSpeed = speedModifier;
                speedModifier = 0;
                //Conductor.instance.NoteHit(owner, isHealing); //temporary call until NoteHeld works
            }

        }
        /*
        if (!Input.anyKey)
        {
            noteHeld = false;
        }
        */

        //note held
        if (noteHeld)
        {
            timeHeld = (MapData.instance.songs.hearingPositionInBeats) - timeHoldStart;
            if (Input.GetKeyUp(keyToPress))
            {
                noteHeld = false;
                Conductor.instance.ReturnMobToIdle(owner);
                gameObject.SetActive(false);
            }
            Conductor.instance.NoteHeld(owner, isHealing, this);
        }

        if (timeHeld > (len * MapData.instance.songs.activeSong.loadMeasureMod) || timeHeld < 0)
        {
            noteHeld = false;
            Conductor.instance.ReturnMobToIdle(owner);
            gameObject.SetActive(false);
        }


        if (timeAlive > timeToLive)
        {
            Destroy(gameObject);
            Resources.UnloadUnusedAssets();
        }
    }

    void Awake()
    {
        timeAlive = 0f;
        timeToLive = 12f;
        isHealing = false;
        wasHit = false;
        noteHeld = false;
        timeHoldStart = 0;
        timeHeld = 0;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Activator")
        {
            canBePressed = true;
        }
    }

    //note missed
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Activator" && gameObject.activeSelf && !wasHit)
        {
            canBePressed = false;
            Conductor.instance.NoteMiss(isHealing);
        }
    }
}
