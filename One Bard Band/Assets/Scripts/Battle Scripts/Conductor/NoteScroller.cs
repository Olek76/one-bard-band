using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
//using System.Diagnostics;

public class NoteScroller : MonoBehaviour
{
    //public Dictionary<KeyCode, GameObject> noteMapping;

    public float noteTempo; //bpm
    public const float baseSpeedModifier = 1.844792f;

    //The number of beats w/in a measure; the top half of the time signature
    public int beatsPerMeasure;

    //Modifier to adjust how many measures ahead notes will appear
    public int loadMeasureMod;

    //The size of each beat w/r.t. a measure; the bottom half of the time
    //  signature (1 = whole, 2 = half, 4 = quarter, etc.)
    public int beatQuantity;

    // (DEFUNCT?)Used in calculating time to wait after completing a battle
    public float secToWait;

    //An ordered queue of Phrases that each define when a note/rhythm must
    //  be input by the player, and which input it applies to
    public Queue<Phrase> phrasesStaged;
    public Queue<Phrase> phrasesHeard;
    public Phrase activePhrase;

    //Flag that determines if notes being loaded will be healing notes or not
    public bool nowHealing;

    public List<GameObject> notesInPlay;

    //Number displayed when notes don't appear immediately
    public int measureCountdown;

    //Button Controller determines which buttons the player can give inputs to
    //  based on the current instrument
    //public ButtonController buttonCtrl;

    public int curEnemy = 0;
    public int aliveEnemies;

    public bool waitActive = false; //so wait function wouldn't be called many times per frame

    public bool hasStarted;

    //wait 2 seconds, then switch scene to victory
    IEnumerator Victory()
    {

        waitActive = true;

        yield return new WaitForSeconds(2f);
        //yield return new WaitForSeconds(secToWait);
        //yield return new WaitForSeconds(0.5f);

        //yield return new WaitForSeconds(BeatToSec(MapData.instance.songs.activeSong.beatsPerMeasure));


        MapData.instance.ReloadMap();

        waitActive = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        //noteTempo = 120f;
        //baseSpeedModifier = 0.2375f;
        //  4 beats to scroll: 0.461197916f
        //  5 beats to scroll: 0.3689583f

        try
        {
            if (Conductor.instance.isBoss)
            {
                noteTempo = MapData.instance.songs.bossSong.tempo;
                beatsPerMeasure = MapData.instance.songs.bossSong.beatsPerMeasure;
            }
            else
            {
                noteTempo = MapData.instance.songs.battleSong.tempo;
                beatsPerMeasure = MapData.instance.songs.battleSong.beatsPerMeasure;
            }

            secToWait = BeatToSec(MapData.instance.songs.activeSong.beatsPerMeasure);
            loadMeasureMod = MapData.instance.songs.activeSong.loadMeasureMod;

        }
        catch (NullReferenceException)
        {
            Debug.Log("No MapData exists");
            noteTempo = 120;
            beatsPerMeasure = 4;
            secToWait = 0f;
        }

        aliveEnemies = Conductor.instance.numEnemies;

    }

    // Update is called once per frame
    void Update()
    {

        bool victory = (aliveEnemies == 0);
        if (victory)
        {
            StartCoroutine(Victory());
        }

        //Debug.Log("measureCountdown: " + measureCountdown);

    }

    void Awake()
    {
        phrasesStaged = new Queue<Phrase>();
        phrasesHeard = new Queue<Phrase>();

        //phrasesHeard.Enqueue(new Phrase(length: beatsPerMeasure));

        hasStarted = false;
        nowHealing = false;
        measureCountdown = 0;

        LoadPhraseFromStage();
    }

    public float SecToBeat(float seconds)
    {
        return seconds * (noteTempo / 60f);
    }

    public float BeatToSec(float beats)
    {
        return beats * (60f / noteTempo);
    }


    public void LoadNote(double hearingBeat, double loadingBeat)
    {
        List<int> hearToRemove = new List<int>();
        List<int> loadToRemove = new List<int>();

        for (int i = 0; i < activePhrase.pattern.Count; i++)
        {
            Note n = activePhrase.pattern[i];
         
            //Checks if note has been heard yet
            if (!n.isHeard)
            {
                //Debug.Log("n.beat == " + n.beat + " | hearingBeat == " + hearingBeat + " | SecToBeat(Time.deltaTime) == " + SecToBeat(Time.deltaTime));
                if (n.beat <= hearingBeat && aliveEnemies > 0)
                {
                    if (n.beat <= hearingBeat)
                        Conductor.instance.enemyScripts[curEnemy].Attack(n.key);
                    hearToRemove.Add(i);
                    //Debug.Log("Note Played at hearing position: " + hearingBeat + " ||| loading position: " + loadingBeat);
                }
            }

            //Checks if note has been loaded yet
            if (!n.isLoaded)
            {
                if (n.beat <= loadingBeat && aliveEnemies > 0)
                {
                    GameObject newNote = Instantiate(Conductor.instance.bardScript.noteMapping[n.key]);

                    newNote.GetComponent<NoteObject>().owner = curEnemy;
                    // set note movement vars to appropriate values defined in NoteScroller
                    newNote.GetComponent<NoteObject>().tempo = noteTempo;
                    newNote.GetComponent<NoteObject>().speedModifier = baseSpeedModifier / (beatsPerMeasure * loadMeasureMod);

                    newNote.GetComponent<NoteObject>().len = n.length;
                    if (n.length > 0)
                    {
                        newNote.GetComponent<NoteObject>().isSustained = true;
                        newNote.GetComponent<NoteObject>().len = newNote.GetComponent<NoteObject>().len / loadMeasureMod;
                    }

                    if (nowHealing)
                    {
                        newNote.GetComponent<NoteObject>().SetHealing();
                    }
                    notesInPlay.Add(newNote);
                    loadToRemove.Add(i);

                    //Debug.Log("Note Played at hearing position: " + hearingBeat + " ||| loading position: " + loadingBeat);
                }
            }

        }

        foreach (int idx in loadToRemove)
        {
            activePhrase.pattern[idx].SetIsLoaded(true);
        }

        foreach (int idx in hearToRemove)
        {
            activePhrase.pattern[idx].SetIsHeard(true);
        }

        loadToRemove.Clear();
        hearToRemove.Clear();

        measureCountdown = (int) -Math.Floor(loadingBeat / beatsPerMeasure);

    }



    //Moves phrase in front of Staged queue to end of OnDeck queue,
    //  in doing so can load appropriate notes
    public void LoadPhraseFromStage()
    {
        //update curEnemy
        try
        {
            curEnemy = GetNextEnemy(curEnemy);
            if (phrasesStaged.Count < 2)
            {
                phrasesStaged.Enqueue(new Phrase(Conductor.instance.enemyScripts[curEnemy].GetAttack()));
                //phrasesStaged.Enqueue(new Phrase(Conductor.instance.enemyScripts[curEnemy].GetAttack()));
            }

            activePhrase = phrasesStaged.Dequeue();
            phrasesHeard.Enqueue(activePhrase);
            //loadingPhrase = new Phrase(activePhrase);
            //loadingPhraseSet = false;
            nowHealing = false;
            notesInPlay.Clear();
        } catch (NullReferenceException)
        {
            Debug.Log("No enemies to load from");
        }



    }


    //returns index of next alive enemy, or the same enemy in the case of 1 enemy alive
    public int GetNextEnemy(int curEnemy)
    {
        int nextEnemy = curEnemy;
        int enemiesLooped = 0;

        do
        {
            nextEnemy = (nextEnemy + 1) % Conductor.instance.numEnemies;
            enemiesLooped++;
        }
        while (Conductor.instance.enemyScripts[nextEnemy].isDead && enemiesLooped < Conductor.instance.numEnemies);
        return nextEnemy;

    }

    public void SetNowHealing(bool healing)
    {
        nowHealing = healing;
    }

}
