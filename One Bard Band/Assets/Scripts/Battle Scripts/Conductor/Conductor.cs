﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Conductor : MonoBehaviour
{
    //Conductor instance
    public static Conductor instance;
    public static Dictionary<KeyCode, InstrumentCode> buttonInstMap;

    //an AudioSource attached to this GameObject that will play the music.
    public AudioSource musicSource;

    //The offset to the first beat of the song in seconds
    public double firstBeatOffset;

    public NoteScroller noteScroll;
    public bool startPlaying;
    public bool isBoss;

    public double notesHit = 0;
    public double notesMissed = 0;

    //Dictionaries of relevant inputs and:
    //      whether they are currently pressed (true) 
    //  or not (false)
    public Dictionary<KeyCode, bool> pressedPassive;
    public Dictionary<KeyCode, bool> pressedThisFrame;

    public KeyCode[] validKeys = { KeyCode.Q, KeyCode.W, KeyCode.E, KeyCode.R, 
                                KeyCode.S, KeyCode.D, KeyCode.F, KeyCode.J, KeyCode.K , KeyCode.L, 
                                KeyCode.C, KeyCode.M};

    //Battle Elements
    public GameObject bardObj;
    public BattleBard bardScript;

    public int numEnemies;
    public GameObject[] enemyObjects;
    public Enemy[] enemyScripts;

    //Initialize is called when a battle is generated. Takes array of strings containing species and constructs encounter (max of 3 enemies)
    public void Initialize(List<string> enemies)
    {
        numEnemies = enemies.Count;
        enemyObjects = new GameObject[numEnemies];
        enemyScripts = new Enemy[numEnemies];

        Component[] renderers;
        //spawn prefabs (positions depend on number of enemies
        switch (numEnemies)
        {
            case 1:
                enemyObjects[0] = (GameObject)Instantiate(Resources.Load(enemies[0]), new Vector3(52.8f, 12f, 0f), Quaternion.identity);
                break;
            case 2:
                enemyObjects[0] = (GameObject)Instantiate(Resources.Load(enemies[0]), new Vector3(52.8f, 12f, 0f), Quaternion.identity);
                enemyObjects[1] = (GameObject)Instantiate(Resources.Load(enemies[1]), new Vector3(30f, 16.1f, 0f), Quaternion.identity);

                //adjust sorting order
                renderers = enemyObjects[1].GetComponentsInChildren<SpriteRenderer>();
                foreach (SpriteRenderer renderer in renderers)
                {
                    renderer.sortingOrder -= 10;
                }
                break;
            case 3:
                enemyObjects[0] = (GameObject)Instantiate(Resources.Load(enemies[0]), new Vector3(52.8f, 11f, 0f), Quaternion.identity);
                enemyObjects[1] = (GameObject)Instantiate(Resources.Load(enemies[1]), new Vector3(30f, 17.1f, 0f), Quaternion.identity);
                enemyObjects[2] = (GameObject)Instantiate(Resources.Load(enemies[2]), new Vector3(8.1f, 12.7f, 0f), Quaternion.identity);

                //adjust sorting order
                renderers =  enemyObjects[1].GetComponentsInChildren<SpriteRenderer>();
                foreach (SpriteRenderer renderer in renderers)
                {
                    renderer.sortingOrder -= 20;
                }
                renderers = enemyObjects[2].GetComponentsInChildren<SpriteRenderer>();
                foreach (SpriteRenderer renderer in renderers)
                {
                    renderer.sortingOrder -= 10;
                }
                break;
            default:
                Debug.Log("Conductor.Initialize only handles combat scenes with 1-3 enemies");
                break;
        }
        //get scripts
        for (int i = 0; i < this.numEnemies; i++)
        {
            enemyScripts[i] = (Enemy)enemyObjects[i].GetComponent("Enemy");
            enemyScripts[i].id = i;
            enemyScripts[i].LoadAttacks();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //Load the AudioSource attached to the Conductor GameObject
        //musicSource = GetComponent<AudioSource>();
    }

    public void StopAllEnemySounds()
    {
        foreach (Enemy e in enemyScripts)
        {
            e.StopAllSounds();
        }
    }


    public void TogglePause()
    {
        bardScript.TogglePause();
        foreach (Enemy e in enemyScripts)
        {
            e.TogglePauseSounds();
        }
    }


    // Update is called once per frame
    void Update()
    {
        
        if (!startPlaying)
        {
            startPlaying = true;
            noteScroll.hasStarted = true;

        } 
        else
        {
            //Get player input
            foreach (KeyCode key in validKeys)
            {
                //Debug.Log("Checking each key");
                if (Input.GetKeyDown(key) && pressedPassive[key] == false)
                {
                    Debug.Log("Key press this frame!");
                    pressedThisFrame[key] = true;
                    pressedPassive[key] = true;
                }

                if (Input.GetKeyUp(key))
                {
                    Debug.Log("Key released");
                    pressedPassive[key] = false;
                }
            }

            //Clear out pressedThisFrame
            foreach (KeyCode key in validKeys)
            {
                pressedThisFrame[key] = false;
            }
        }
    }

    void Awake()
    {
        Time.timeScale = 1f;

        MapData.instance.songs.StartBattleMusic();

        instance = this;
        buttonInstMap = new Dictionary<KeyCode, InstrumentCode>();

        buttonInstMap.Add(KeyCode.Z, InstrumentCode.Bongo);
        buttonInstMap.Add(KeyCode.X, InstrumentCode.Bongo);

        buttonInstMap.Add(KeyCode.A, InstrumentCode.PanFlute);
        buttonInstMap.Add(KeyCode.S, InstrumentCode.PanFlute);
        buttonInstMap.Add(KeyCode.D, InstrumentCode.PanFlute);
        buttonInstMap.Add(KeyCode.F, InstrumentCode.PanFlute);

        buttonInstMap.Add(KeyCode.Q, InstrumentCode.Lute);
        buttonInstMap.Add(KeyCode.W, InstrumentCode.Lute);
        buttonInstMap.Add(KeyCode.E, InstrumentCode.Lute);
        buttonInstMap.Add(KeyCode.R, InstrumentCode.Lute);
        buttonInstMap.Add(KeyCode.T, InstrumentCode.Lute);

        notesHit = 0;
        notesMissed = 0;

        //generate battle elements
        bardObj = (GameObject)Instantiate(Resources.Load("BattleBard"));

        bardScript = (BattleBard) bardObj.GetComponent("BattleBard");

        //List<string> initializationArray = { "Enemies/GoblinChief" };
        //List<string> initializationArray = new List<string>{ "Enemies/GoblinA" }, "Enemies/GoblinB", "Enemies/GoblinC"};
        List<string> defaultEnemyArray = new List<string> { "Enemies/GoblinA" };


        //Load Enemies
        try
        {
            Initialize(MapData.instance.GetEnemies());
        } catch (NullReferenceException)
        {
            Debug.Log("Map doesn't exist, scene was prob loaded straight from Unity Editor");
            Initialize(defaultEnemyArray);
        }


        // Load Music
        isBoss = false;
        try
        {

            // Check for boss to set appropriate music
            foreach (GameObject enemy in enemyObjects)
            {
                if (enemy.CompareTag("Boss"))
                {
                    isBoss = true;
                }
            }

            if (isBoss)
            {
                Debug.Log("GASP!");
                //MapData.instance.songs.StartBossMusic();
                MapData.instance.songs.PrepBossMusic();
                
                musicSource = MapData.instance.songs.bossSong.audioSrc;

                GameObject bossPitches = GameObject.FindWithTag("BossPitches");
                AudioSource[] pitches = bossPitches.GetComponents<AudioSource>();
                bardScript.SetSounds(pitches);

                Debug.Log("It's a boss!!");
            } else
            {
                musicSource = MapData.instance.songs.battleSong.audioSrc;
            }

        } catch (NullReferenceException)
        {
            musicSource = GetComponent<AudioSource>();
        }


        //Init consistent variables:
        //  KeyPress Dictionaries
        pressedPassive = new Dictionary<KeyCode, bool>();
        pressedThisFrame = new Dictionary<KeyCode, bool>();

        foreach (KeyCode key in validKeys)
        {
            pressedPassive[key] = false;
            pressedThisFrame[key] = false;
        }

        startPlaying = false;
    }

    public void NoteHit(int owner, bool isHealing)
    {
        if (!isHealing)
        {
            //take damage
            enemyScripts[owner].TakeDamage(1, false);
        }
        else
        {
            //heal bard
            bardScript.Heal(1);
        }
    }
    //sustained note is held
    //should periodically heal / hurt as it is being held
    public void NoteHeld(int owner, bool isHealing, NoteObject note)
    {
        if (!isHealing)
        {
            //take damage
            enemyScripts[owner].TakeDamage(0.025f, true);
        }
        else
        {
            //heal bard
            bardScript.Heal(0.025f);
        }
    }

    public void ReturnMobToIdle(int owner)
    {
        StartCoroutine(enemyScripts[owner].ReturnToIdle());
    }

    public void NoteMiss(bool isHealing)
    {
        if (!isHealing)
        {
            notesMissed++;
            bardScript.TakeDamage(1);
        }
    }
}
