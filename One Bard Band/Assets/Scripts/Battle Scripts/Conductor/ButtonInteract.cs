﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonInteract : MonoBehaviour
{

    private SpriteRenderer sr;
    public Sprite defImg;
    public Sprite pressImg;
    public KeyCode keyToPress;

    public bool multiKeyButton;
    public KeyCode[] keysToPress;


    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        
        if (multiKeyButton)
        {
            // may want to replace with loading based on what instruments the player has
            keysToPress = new KeyCode[] { KeyCode.Q, KeyCode.W, KeyCode.E, KeyCode.R, KeyCode.S, 
                                            KeyCode.D, KeyCode.F, KeyCode.J, KeyCode.K, KeyCode.L,
                                            KeyCode.C, KeyCode.M};
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (multiKeyButton)
        {
            foreach(KeyCode multikey in keysToPress)
            {


                if (Input.GetKeyDown(keyToPress) || Input.GetKeyDown(multikey) )
                {
                    sr.sprite = pressImg;
                }

                if (Input.GetKeyUp(keyToPress) || Input.GetKeyUp(multikey))
                {
                    sr.sprite = defImg;
                }

        
            }

        }   

    }
}
