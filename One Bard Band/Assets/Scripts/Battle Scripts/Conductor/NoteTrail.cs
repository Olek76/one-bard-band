﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteTrail : MonoBehaviour
{
    public SpriteRenderer sRenderer;
    public Sprite healingSprite;
    private double initWidth; //initial width of trail sprite at x scale of 1 (game space)
    public double scaleFactor; //scale to stretch to note length
    public double measureLen; //length of measure from where notes spawn to button
    public float trailSpeed; //when note is stopped but trail continues, trailSpeed becomes note's speedmodifier

    // Start is called before the first frame update
    void Start()
    {
        //sets scale of trail
        if (transform.parent.GetComponent<NoteObject>().len > 0)
        {
            //Debug.Log(sprite.bounds.size.x); 
            initWidth = sRenderer.bounds.size.x;
            measureLen = 115.8;
            //scaleFactor = (Length_of_measure * note_len_in_beats) / (initWidth * beats_per_measure)
            scaleFactor = (measureLen * transform.parent.GetComponent<NoteObject>().len) / (initWidth * Conductor.instance.noteScroll.beatsPerMeasure);
            sRenderer.transform.localScale = new Vector3((float)scaleFactor, .25f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (trailSpeed > 0)
        {
            transform.position -= new Vector3(transform.parent.GetComponent<NoteObject>().tempo * trailSpeed * Time.deltaTime, 0f);
        }
        if (transform.parent.GetComponent<NoteObject>().isHealing)
        {
            sRenderer.sprite = healingSprite;
        }
    }
}
