﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{

    //public bool IsActive;
    public Dictionary<InstrumentCode, GameObject> instButtonMap;


    // current Inst and Buttons privatized to prevent mismatch; currentInst
    //  can only be changed through SetInstrumentCode(), which will then trigger
    //  the if clause in Update() that changes currentButtons;
    private InstrumentCode currentInst;
    private GameObject currentButtons;

    // Start is called before the first frame update
    void Start()
    {

        currentInst = /*new InstrumentCode(*/InstrumentCode.Lute;

      /*  if (Conductor.instance.bardScript.hasLute)
        {
            instButtonMap.Add(currentInst, Resources.Load("Button Prefabs/LuteButtons") as GameObject);
        }
        *///Add other conditionally existent InstrumentCode-ButtonGroup mappings here


        //Should change so there's no duplicate new InstrumentCode()
        currentButtons = instButtonMap[currentInst];


        Instantiate(currentButtons);
    }

    // Update is called once per frame
    void Update()
    {

        if (instButtonMap[currentInst] != currentButtons)
        {
            Destroy(currentButtons);
            currentButtons = instButtonMap[currentInst];
            Instantiate(currentButtons);

        }
    }

    // Initialize InstrumentCode to button group mapping
    void Awake()
    {
        instButtonMap = new Dictionary<InstrumentCode, GameObject>();
        //instButtonMap.Add(InstrumentCode.Bongo, Resources.Load("Button Prefabs/BongoButtons"));
        instButtonMap.Add(InstrumentCode.PanFlute, Resources.Load("Button Prefabs/PanFluteButtons") as GameObject);
        instButtonMap.Add(InstrumentCode.Lute, Resources.Load("Button Prefabs/LuteButtons") as GameObject);

    }

    public void SetInstrumentCode(InstrumentCode inst)
    {
        currentInst = inst;
    }

    public InstrumentCode GetInstrumentCode()
    {
        return currentInst;
    }

}
