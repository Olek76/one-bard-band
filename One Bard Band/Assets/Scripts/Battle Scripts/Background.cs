﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    public Sprite camp;
    public Sprite cave;


    // Start is called before the first frame update
    void Start()
    {
        if (MapData.instance.levelName == "GoblinCamp")
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = camp;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = cave;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
