﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Boss differs from mob in that it:
//1. doesn't have a health bar (it's defeated if the Bard survives the song)
//2. has a set pattern of rhythms that are loaded in order
//3. Phrases might use multiple different instruments
public class Boss : Enemy
{
    public int curPhrase = 0;
    public int numPhrases;

    public double songLength; //length of song in seconds
    public double songLengthInBeats; //length of song in beats

    public bool songFinished;

    private GameObject bardObj;
    private BattleBard bardScript;

    //return next attack, increment curPhrase if there are no more phrases, return empty phrase, call SongEnd
    public override Phrase GetAttack()
    {
        if (curPhrase < numPhrases)
        {
            return attacks[curPhrase++];
        }
        else
        {
            if (!songFinished)
            {
                SongEnd();
            }
            return new Phrase();
        }
    }
    public void SongEnd()
    {
        songFinished = true;
        if (!bardScript.isDead)
        {
            StartCoroutine(WaitForEnd());
        }
}

    //wait last phrase length + 2 seconds
    IEnumerator WaitForEnd()
    {
        waitActive = true;
        float phraseLength = attacks[curPhrase - 1].length;
        float phraseLengthInSecs = (phraseLength * 60) / Conductor.instance.noteScroll.noteTempo;
        yield return new WaitForSeconds(2 + phraseLengthInSecs);
        Instantiate(Resources.Load("Menu/BattleResults"));
        waitActive = false;
    }

    public override void TakeDamage(float damage, bool held)
    {
        spriteState = 1;
        newState = true;

        if (!waitActive && !held)
        {
            StartCoroutine(ReturnToIdle());
        }
    }

    void Awake()
    {
        songFinished = false;
        bardObj = GameObject.FindWithTag("Player");
        bardScript = bardObj.GetComponent<BattleBard>();
    }

}
