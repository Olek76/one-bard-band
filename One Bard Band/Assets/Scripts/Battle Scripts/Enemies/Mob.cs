﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Mob differs from Boss in that it:
//1. has a health bar 
//2. has a set of rhythms that are loaded randomly
public class Mob : Enemy
{
    System.Random rand;

    public HealthBar barScript;

    public string species; //this must be a string that is identical to its prefab's name

    //return random attack
    public override Phrase GetAttack()
    {
        if (rand == null)
        {
            rand = new System.Random();
        }
        int idx = rand.Next(attacks.Count);
        return attacks[idx];
    }

    public void Die()
    {
        //I hate aliveEnemies
        if (!isDead)
        {
            isDead = true;
            spriteState = 2;
            newState = true;
            Conductor.instance.noteScroll.aliveEnemies -= 1;
            //Debug.Log("Live enemies " + Conductor.instance.noteScroll.aliveEnemies);
            Conductor.instance.noteScroll.SetNowHealing(true);


            //find remaining notes, set as healing notes
            GameObject[] notes;
            notes = GameObject.FindGameObjectsWithTag("Note");
            foreach (GameObject note in notes)
            {
                if (note.GetComponent<NoteObject>().owner == id)
                {
                    note.GetComponent<NoteObject>().SetHealing();
                }
            }

            GetComponentInChildren<HealthBar>().DestroyBar();
        }
    }

    public override void TakeDamage(float damage, bool held)
    {
        if (!this.isDead)
        {
            spriteState = 1;
            newState = true;

            curHealth -= damage;
            float healthPercent = this.curHealth / this.maxHealth;
            barScript.SetSize(healthPercent);

            if (this.curHealth <= 0.0)
            {
                Die();
            }
            else if (!waitActive && !held)
            {
                StartCoroutine(ReturnToIdle());
            }
        }
    }
}
