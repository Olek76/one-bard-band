﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.IO;

public class Siren : Mob
{
    public AudioSource audioRoot;  // L note sound
    public AudioSource audio2nd;   // K
    public AudioSource audio3rd;   // J
    public AudioSource audio5th;   // F
    public AudioSource audio6th;   // D
    public AudioSource audioOct;   // S


    //Populates attacks with all patterns of enemy type
    public override void LoadAttacks()
    {
        attacks = new List<Phrase>();
        List<Note> notes;

        patternPath = "Patterns/Siren/" + species;
        TextAsset fileText = (TextAsset)Resources.Load(patternPath, typeof(TextAsset));
        string[] lines = fileText.text.Split(new string[] { "\r\n" }, StringSplitOptions.None);

        //iterate over each line "phrase" from pattern .txt file, adding them to attacks<Phrases>
        foreach (string line in lines)
        {
            //parsedLine[0] : Phrase.length 
            //parsedLine[1] : phrase
            string[] parsedLine = line.Split('|');

            notes = new List<Note>();

            //iterate over each note in noteData
            //note[0] : beat 
            //note[1] : keycode 
            //note[2] : note length (in beats) 0 means non sustained
            string[] noteData = parsedLine[1].Split(' ');
            foreach (string n in noteData)
            {
                string[] note = n.Split(',');
                notes.Add(new Note(beat: Convert.ToDouble(note[0]), inputKey: StringToKeyCode(note[1]), length: Convert.ToDouble(note[2])));
            }
            attacks.Add(new Phrase(notes, Convert.ToInt32(parsedLine[0]), id));
        }
    }

    private KeyCode StringToKeyCode(string code)
    {
        switch (code)
        {
            case "s":
                return KeyCode.S;
            case "d":
                return KeyCode.D;
            case "f":
                return KeyCode.F;
            case "j":
                return KeyCode.J;
            case "k":
                return KeyCode.K;
            case "l":
                return KeyCode.L;
            default:
                return KeyCode.Space;
        }
    }

    //called at end of phrase in MusicManager
    public override void StopAllSounds()
    {
        audioRoot.Stop();
        audio2nd.Stop();
        audio3rd.Stop();
        audio5th.Stop();
        audio6th.Stop();
        audioOct.Stop();

        if (!waitActive && !isDead)
        {
            StartCoroutine(ReturnToIdle());
        }

    }

    //called between notes
    public void StopAllSoundsMidPhrase()
    {
        audioRoot.Stop();
        audio2nd.Stop();
        audio3rd.Stop();
        audio5th.Stop();
        audio6th.Stop();
        audioOct.Stop();
    }

    public override void TogglePauseSounds()
    {
        paused = !paused;

        AudioSource[] allAudio = {audioRoot, audio2nd, audio3rd, audio5th, audio6th, audioOct};

        if (paused)
        {
            /*foreach (AudioSource a in allAudio)
            {
                a.Pause();
            }*/

            audioRoot.Pause();
            audio2nd.Pause();
            audio3rd.Pause();
            audio5th.Pause();
            audio6th.Pause();
            audioOct.Pause();

        } else
        {
            foreach (AudioSource a in allAudio)
            {
                a.UnPause();
            }
        }
    }


    //called on beats where enemy produces notes
    public override void Attack(KeyCode key)
    {
        if (!isDead)
        {
            StopAllSoundsMidPhrase();
            
            if (key == KeyCode.S)
            {
                audioOct.Play();
            }
            else if (key == KeyCode.D)
            {
                audio6th.Play();
            }
            else if (key == KeyCode.F)
            {
                audio5th.Play();
            }
            else if (key == KeyCode.J)
            {
                audio3rd.Play();
            }
            else if (key == KeyCode.K)
            {
                audio2nd.Play();
            }
            else if (key == KeyCode.L)
            {
                audioRoot.Play();
            }
            if (spriteState != 3)
            {
                spriteState = 3;
                newState = true;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        spriteState = 0;
        newState = true;
        paused = false;
        
        AudioSource[] audioSources = GetComponents<AudioSource>();
        audioRoot = audioSources[0];
        audio2nd = audioSources[1];
        audio3rd = audioSources[2];
        audio5th = audioSources[3];
        audio6th = audioSources[4];
        audioOct = audioSources[5];
        
    }

    void Update()
    {
        if (newState)
        {
            animator.SetInteger("State", spriteState);
            newState = false;
            switch (spriteState)
            {
                case 1:
                    //hit, then return to idle
                    animator.Play("Base Layer.Hurt", -1, 0);
                    break;
                case 2:
                    //die
                    animator.Play("Base Layer.Death", -1, 0);

                    //healthBar.SetActive(false);
                    //gameObject.active = false;
                    break;
                case 3:
                    //attack
                    animator.Play("Base Layer.Hit", -1, 0);
                    break;
                default:
                    //idle
                    animator.Play("Base Layer.Idle", -1, 0);
                    break;
            }
        }
    }
}
