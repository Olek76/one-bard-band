﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.IO;

public class Goblin : Mob
{
    public AudioSource attackAudioLow;
    public AudioSource attackAudioHigh;

    //Populates attacks with all patterns of enemy type
    public override void LoadAttacks()
    {
        attacks = new List<Phrase>();
        List<Note> notes;

        if (MapData.instance.levelName == "SirenCave")
        {
            patternPath = "Patterns/Goblin/Cave" + species;
        }
        else
        {
            patternPath = "Patterns/Goblin/" + species;
        }
        
        TextAsset fileText = (TextAsset)Resources.Load(patternPath, typeof(TextAsset));
        string[] lines = fileText.text.Split(new string[] { "\r\n" }, StringSplitOptions.None);

        //iterate over each line "phrase" from pattern .txt file, adding them to attacks<Phrases>
        foreach (string line in lines)
        {
            //parsedLine[0] = Phrase.length 
            //parsedLine[1] = phrase
            string[] parsedLine = line.Split('|');

            notes = new List<Note>();
            //iterate over each pair parsedLine[0] = beat parsedLine[1] = keycode
            string[] pairs = parsedLine[1].Split(' ');
            foreach (string pair in pairs)
            {
                string[] note = pair.Split(',');
                notes.Add(new Note(beat: Convert.ToDouble(note[0]), inputKey: StringToKeyCode(note[1])));
            }
            attacks.Add(new Phrase(notes, Convert.ToInt32(parsedLine[0]), id));
        }
    }

    private KeyCode StringToKeyCode(string code)
    {
        switch (code)
        {
            case "c":
                return KeyCode.C;
            case "m":
                return KeyCode.M;
            default:
                return KeyCode.Space;
        }
    }

    //called on beats where enemy produces notes. Plays attack animation
    public override void Attack(KeyCode key)
    {
        if (!this.isDead)
        {
            if (key == KeyCode.C)
            {
                attackAudioHigh.Play();
            }
            else if (key == KeyCode.M)
            {
                attackAudioLow.Play();
            }
            spriteState = 3;
            newState = true;
            if (!waitActive)
            {
                StartCoroutine(ReturnToIdle());
            }
        }
    }

    public override void TogglePauseSounds()
    {
        paused = !paused;

        AudioSource[] allAudio = { attackAudioHigh, attackAudioLow };

        if (paused)
        {
            foreach (AudioSource a in allAudio)
            {
                a.Pause();
            }
        }
        else
        {
            foreach (AudioSource a in allAudio)
            {
                a.UnPause();
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        spriteState = 0;
        newState = true;
        paused = false;

        AudioSource[] audioSources = GetComponents<AudioSource>();
        attackAudioLow = audioSources[0];
        attackAudioHigh = audioSources[1];
    }

    void Update()
    {
        if (newState)
        {
            animator.SetInteger("State", spriteState);
            newState = false;
            switch (spriteState)
            {
                case 1:
                    //hit, then return to idle
                    animator.Play("Base Layer.Hurt", -1, 0);
                    break;
                case 2:
                    //die
                    animator.Play("Base Layer.Death", -1, 0);
                    break;
                case 3:
                    //attack, then return to idle
                    animator.Play("Base Layer.Hit", -1, 0);
                    break;
                default:
                    //idle
                    animator.Play("Base Layer.Idle", -1, 0);
                    break;
            }
        }
    }
}
