﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.IO;

//Parent class
public class Enemy : MonoBehaviour
{
    public float maxHealth, curHealth;
    public bool isDead, paused;

    public int spriteState;
    public bool newState;
    public Animator animator;

    public bool waitActive = false; //so wait function wouldn't be called many times per frame

    public string patternPath; //filepath to file with attack patterns
    public int id;

    public List<Phrase> attacks;    //attack phrases

    //wait .25 seconds, then return sprite to normal
    public IEnumerator ReturnToIdle()
    {
        waitActive = true;
        yield return new WaitForSeconds(0.0625f);
        if (!isDead)
        {
            spriteState = 0;
            newState = true;
        }
        waitActive = false;
    }

    //member functions overriden by children classes
    public virtual Phrase GetAttack() { return new Phrase(); }
    public virtual void LoadAttacks() {}
    public virtual void Attack(KeyCode key) {}
    public virtual void TakeDamage(float damage, bool held) {}
    public virtual void StopAllSounds() {}
    public virtual void TogglePauseSounds() {}
}
