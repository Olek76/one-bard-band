﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class SirenQueen : Boss
{
    public AudioSource bongoLow; // m
    public AudioSource bongoHigh; //c 
    public AudioSource pipeRoot;  // L note sound
    public AudioSource pipe2nd;   // K
    public AudioSource pipe3rd;   // J
    public AudioSource pipe5th;   // F
    public AudioSource pipe6th;   // D
    public AudioSource pipeOct;   // S

    //Populates attacks with all patterns of enemy type
    public override void LoadAttacks()
    {
        attacks = new List<Phrase>();
        List<Note> notes;

        songLengthInBeats = 0;

        patternPath = "Patterns/Bosses/SirenQueen";
        TextAsset fileText = (TextAsset)Resources.Load(patternPath, typeof(TextAsset));
        string[] lines = fileText.text.Split(new string[] { "\r\n" }, StringSplitOptions.None);

        //iterate over each line "phrase" from pattern .txt file, adding them to attacks<Phrases>
        foreach (string line in lines)
        {
            //parsedLine[0] = Phrase.length 
            //parsedLine[1] = phrase
            string[] parsedLine = line.Split('|');

            songLengthInBeats += 2 * Convert.ToDouble(parsedLine[0]);



            if (parsedLine[1] == "_")
            {
                Debug.Log("Empty phrase created");
                attacks.Add(new Phrase(length: Convert.ToInt32(parsedLine[0]), owner: 0));

            }
            else
            {
                notes = new List<Note>();

                //iterate over each pair 
                string[] pairs = parsedLine[1].Split(' ');
                foreach (string pair in pairs)
                {
                    string[] note = pair.Split(',');
                    //Debug.Log("note[0] == " + note[0]);
                    //Debug.Log("note[1] == " + note[1]);
                    //Debug.Log("note[2] == " + note[2]);


                    notes.Add(new Note(beat: Convert.ToDouble(note[0]), inputKey: StringToKeyCode(note[1]), length: Convert.ToDouble(note[2])));
                }
                attacks.Add(new Phrase(notes, Convert.ToInt32(parsedLine[0]), 0));
            }
        }

        numPhrases = attacks.Count;
        songLength = songLengthInBeats / GameObject.FindGameObjectWithTag("Scroll").GetComponent<NoteScroller>().noteTempo;

        //Debug.Log("End of SirenQueen pattern parsing; numPhrases == " + numPhrases + "; songLength == " + songLength);

    }

    //called on beats where enemy produces notes. Plays attack animation
    public override void Attack(KeyCode key)
    {
        if (!this.isDead)
        {
            StopAllSoundsMidPhrase();
            
            if (key == KeyCode.C)
            {
                bongoHigh.Play();

            }
            else if (key == KeyCode.M)
            {
                bongoLow.Play();
            }
            else if (key == KeyCode.S)
            {
                pipeOct.Play();
            }
            else if (key == KeyCode.D)
            {
                pipe6th.Play();
            }
            else if (key == KeyCode.F)
            {
                pipe5th.Play();
            }
            else if (key == KeyCode.J)
            {
                pipe3rd.Play();
            }
            else if (key == KeyCode.K)
            {
                pipe2nd.Play();
            }
            else if (key == KeyCode.L)
            {
                pipeRoot.Play();
            }

            if (spriteState != 3)
            {
                spriteState = 3;
                newState = true;
            }

        }
    }

    private KeyCode StringToKeyCode(string code)
    {
        switch (code)
        {
            case "s":
                return KeyCode.S;
            case "d":
                return KeyCode.D;
            case "f":
                return KeyCode.F;
            case "j":
                return KeyCode.J;
            case "k":
                return KeyCode.K;
            case "l":
                return KeyCode.L;
            case "c":
                return KeyCode.C;
            case "m":
                return KeyCode.M;
            default:
                return KeyCode.Space;
        }
    }

    //called at end of phrase in MusicManager
    public override void StopAllSounds()
    {
        pipeRoot.Stop();
        pipe2nd.Stop();
        pipe3rd.Stop();
        pipe5th.Stop();
        pipe6th.Stop();
        pipeOct.Stop();

        if (!waitActive)
        {
            StartCoroutine(ReturnToIdle());
        }

    }

    //called between notes
    public void StopAllSoundsMidPhrase()
    {
        pipeRoot.Stop();
        pipe2nd.Stop();
        pipe3rd.Stop();
        pipe5th.Stop();
        pipe6th.Stop();
        pipeOct.Stop();
    }


    public override void TogglePauseSounds()
    {
        paused = !paused;

        AudioSource[] allAudio = { pipeRoot, pipe2nd, pipe3rd, pipe5th, pipe6th, pipeOct };

        if (paused)
        {
            foreach (AudioSource a in allAudio)
            {
                a.Pause();
            }
        }
        else
        {
            foreach (AudioSource a in allAudio)
            {
                a.UnPause();
            }
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        //animator = GetComponentInChildren<Animator>();
        spriteState = 0;
        newState = true;

        AudioSource[] audioSources = GetComponents<AudioSource>();
        pipeRoot = audioSources[0];
        pipe2nd = audioSources[1];
        pipe3rd = audioSources[2];
        pipe5th = audioSources[3];
        pipe6th = audioSources[4];
        pipeOct = audioSources[5];
        bongoHigh = audioSources[6];
        bongoLow = audioSources[7];

        Debug.Log("End of SirenQueen Start()");

    }

    void Update()
    {
        if (newState)
        {
            animator.SetInteger("State", spriteState);
            newState = false;
            switch (spriteState)
            {
                case 1:
                    //hit, then return to idle
                    animator.Play("Base Layer.Hurt", -1, 0);
                    break;
                case 2:
                    //die
                    animator.Play("Base Layer.Death", -1, 0);
                    break;
                case 3:
                    //attack, then return to idle
                    animator.Play("Base Layer.Hit", -1, 0);
                    break;
                default:
                    //idle
                    animator.Play("Base Layer.Idle", -1, 0);
                    break;
            }

        }
    }
}
