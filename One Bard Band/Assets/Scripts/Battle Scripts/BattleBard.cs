﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BattleBard : MonoBehaviour
{
    public static float maxHealth = 30;
    public static float curHealth = 30;
    public bool hasBongos, hasLute, hasPipe, isDead, soundsSet;
    public bool paused;

    public int keyUpCount;
    public int spriteState;
    public bool newState;
    public int idleState;

    public GameObject healthBar;
    public HealthBar barScript;

    public AudioSource bongoAudioLow;
    public AudioSource bongoAudioHigh;

    public AudioSource fluteRoot;
    public AudioSource flute2nd;
    public AudioSource flute3rd;
    public AudioSource flute5th;
    public AudioSource flute6th;
    public AudioSource fluteOct;


    public Dictionary<KeyCode, GameObject> noteMapping;

    bool firstFrame = true;

    public Animator animator;

    public bool waitActive = false; //so wait function wouldn't be called many times per frame

    //wait .25 seconds, then return sprite to normal
    IEnumerator ReturnToIdle()
    {
        waitActive = true;
        yield return new WaitForSeconds(0.0625f);
        spriteState = idleState;
        newState = true;
        waitActive = false;
    }

    //wait 2 seconds, then game over
    IEnumerator GameOver()
    {
        waitActive = true;

        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("GameOver");
        MapData.instance.songs.SetGameOverValues();


        waitActive = false;
    }

    //take damage, play hurt animation
    public void TakeDamage(int damage)
    {
        if (!isDead)
        {
            spriteState = idleState + 1; //the hurt sprite state is always immediately after the idle
            newState = true;

            curHealth -= damage;
            //update healthbar
            float healthPercent = curHealth / maxHealth;
            barScript.SetSize(healthPercent);

            if (curHealth <= 0)
            {
                Die();
            }
            else if (!waitActive)
            {
                StartCoroutine(ReturnToIdle());
            }
        }
    }

    public void Heal(float health)
    {
        if (curHealth < maxHealth)
        {
            if (curHealth + health > maxHealth)
            {
                curHealth = maxHealth;
            } else
            {
                curHealth += health;
            }
            //update healthbar
            float healthPercent = curHealth / maxHealth;
            barScript.SetSize(healthPercent);
        }
    }

    public void Die()
    {
        isDead = true;
        spriteState = 2;
        newState = true;
        StartCoroutine(GameOver());
    }

    public void TogglePause()
    {
        paused = !paused;
        if (paused)
        {
            bongoAudioHigh.Pause();
            bongoAudioLow.Pause();
            fluteRoot.Pause();
            flute2nd.Pause();
            flute3rd.Pause();
            flute5th.Pause();
            flute6th.Pause();
            fluteOct.Pause();
        } else
        {
            bongoAudioHigh.UnPause();
            bongoAudioLow.UnPause();
            fluteRoot.UnPause();
            flute2nd.UnPause();
            flute3rd.UnPause();
            flute5th.UnPause();
            flute6th.UnPause();
            fluteOct.UnPause();
        }

    }

    public void StopAllSounds()
    {
        //bongoAudioHigh.Stop();
        //bongoAudioLow.Stop();
        fluteRoot.Stop();
        flute2nd.Stop();
        flute3rd.Stop();
        flute5th.Stop();
        flute6th.Stop();
        fluteOct.Stop();

    }

    //initializes audio sources to correct 
    public void SetSounds(AudioSource[] audioSources)
    {
        if (!soundsSet)
        {
            if (hasBongos && audioSources.Length >= 2)
            {
                bongoAudioHigh = audioSources[0];
                bongoAudioLow = audioSources[1];
            }

            if (hasPipe && audioSources.Length >= 8)
            {
                fluteRoot = audioSources[2];
                flute2nd = audioSources[3];
                flute3rd = audioSources[4];
                flute5th = audioSources[5];
                flute6th = audioSources[6];
                fluteOct = audioSources[7];
            }

            soundsSet = true;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();
        SetSounds(audioSources);

        animator = GetComponentInChildren<Animator>();

        //validKeys = Conductor.instance.noteScroll.noteMapping.Keys;
    }

    void Awake()
    {
        // These bools will in the future be set based on inventory/game progression
        hasBongos = true;
        hasLute = false;
        hasPipe = true;

        soundsSet = false;
        paused = false;

        //set sprite to idle
        spriteState = 0;
        idleState = 0;
        newState = true;

        noteMapping = new Dictionary<KeyCode, GameObject>();
        if (hasBongos)
        {
            noteMapping.Add(KeyCode.C, Resources.Load("Note Prefabs/Note C") as GameObject);
            noteMapping.Add(KeyCode.M, Resources.Load("Note Prefabs/Note M") as GameObject);
        }

        if (hasLute)
        {
            noteMapping.Add(KeyCode.Q, Resources.Load("Note Prefabs/Note Q") as GameObject);
            noteMapping.Add(KeyCode.W, Resources.Load("Note Prefabs/Note W") as GameObject);
            noteMapping.Add(KeyCode.E, Resources.Load("Note Prefabs/Note E") as GameObject);
            noteMapping.Add(KeyCode.R, Resources.Load("Note Prefabs/Note R") as GameObject);
        }

        if (hasPipe)
        {
            noteMapping.Add(KeyCode.S, Resources.Load("Note Prefabs/Note S") as GameObject);
            noteMapping.Add(KeyCode.D, Resources.Load("Note Prefabs/Note D") as GameObject);
            noteMapping.Add(KeyCode.F, Resources.Load("Note Prefabs/Note F") as GameObject);
            noteMapping.Add(KeyCode.J, Resources.Load("Note Prefabs/Note J") as GameObject);
            noteMapping.Add(KeyCode.K, Resources.Load("Note Prefabs/Note K") as GameObject);
            noteMapping.Add(KeyCode.L, Resources.Load("Note Prefabs/Note L") as GameObject);
        }


    }

    // Update is called once per frame
    void Update()
    {
        //newSpriteState = 1; //sets spriteState to assoc w/ idle

        if (firstFrame)
        {
            float healthPercent = curHealth / maxHealth;
            barScript.SetSize(healthPercent);
            firstFrame = false;
        }

        if (!this.isDead && !paused)
        {
            foreach (KeyCode k in noteMapping.Keys)
            {
                if (Input.GetKeyDown(k))
                {
                    if (k == KeyCode.C)
                    {
                        bongoAudioHigh.Play();
                        spriteState = 5;
                        idleState = 3;
                        newState = true;
                    }
                    else if (k == KeyCode.M)
                    {
                        bongoAudioLow.Play();
                        spriteState = 6;
                        idleState = 3;
                        newState = true;
                    }
                    else if (k == KeyCode.S)
                    {
                        fluteOct.Play();
                        spriteState = 9;
                        idleState = 7;
                        newState = true;
                    }
                    else if (k == KeyCode.D)
                    {
                        flute6th.Play();
                        spriteState = 9;
                        idleState = 7;
                        newState = true;
                    } 
                    else if (k == KeyCode.F)
                    {
                        flute5th.Play();
                        spriteState = 9;
                        idleState = 7;
                        newState = true;
                    }
                    else if (k == KeyCode.J)
                    {
                        flute3rd.Play();
                        spriteState = 9;
                        idleState = 7;
                        newState = true;
                    }
                    else if (k == KeyCode.K)
                    {
                        flute2nd.Play();
                        spriteState = 9;
                        idleState = 7;
                        newState = true;
                    }
                    else if (k == KeyCode.L)
                    {
                        fluteRoot.Play();
                        spriteState = 9;
                        idleState = 7;
                        newState = true;
                    }



                }

                if (Input.GetKeyUp(k))
                {
                    //spriteState = idleState;
                    if (k == KeyCode.C || k == KeyCode.M)
                    {
                        StartCoroutine(ReturnToIdle());
                    }

                    if (k == KeyCode.S)
                    {
                        fluteOct.Stop();
                        StartCoroutine(ReturnToIdle());
                    }
                    if (k == KeyCode.D)
                    {
                        flute6th.Stop();
                        StartCoroutine(ReturnToIdle());
                    }
                    if (k == KeyCode.F)
                    {
                        flute5th.Stop();
                        StartCoroutine(ReturnToIdle());
                    }
                    if (k == KeyCode.J)
                    {
                        flute3rd.Stop();
                        StartCoroutine(ReturnToIdle());
                    }
                    if (k == KeyCode.K)
                    {
                        flute2nd.Stop();
                        StartCoroutine(ReturnToIdle());
                    }
                    if (k == KeyCode.L)
                    {
                        fluteRoot.Stop();
                        StartCoroutine(ReturnToIdle());
                    }
                }
            }

        }

        if (!Input.anyKey)
        {
            StopAllSounds();
        }

        if (newState)
        {
            animator.SetInteger("State", spriteState);
            newState = false;
            switch (spriteState)
            {
                case 1:
                    //hurt w/o instruments
                    animator.Play("Base Layer.Hurt", -1, 0);
                    break;
                case 2:
                    //die
                    animator.Play("Base Layer.Death", -1, 0);
                    break;
                case 3:
                    //bongo idle
                    animator.Play("Base Layer.Bongo Idle", -1, 0);
                    break;
                case 4:
                    //hit, then return to idle
                    animator.Play("Base Layer.Bongo Hurt", -1, 0);
                    break;
                case 5:
                    //high bongo, then return to idle
                    animator.Play("Base Layer.Bongo High", -1, 0);
                    break;
                case 6:
                    //low bongo, then return to idle
                    animator.Play("Base Layer.Bongo Low", -1, 0);
                    break;
                case 7:
                    //pipe idle
                    animator.Play("Base Layer.Flute Idle", -1, 0);
                    break;
                case 8:
                    //pipe hurt
                    animator.Play("Base Layer.Flute Hurt", -1, 0);
                    break;
                case 9:
                    //pipe play
                    animator.Play("Base Layer.Flute Hit", -1, 0);
                    break;
                default: // case 0
                    //idle
                    animator.Play("Base Layer.Idle", -1, 0);
                    break;
            }
            
        }
    }
}
