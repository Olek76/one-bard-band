﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class BattleResults : MonoBehaviour
{

    public Text statistics;

    // Start is called before the first frame update
    void Start()
    {
        MapData.instance.songs.StopMusic();

        statistics = GameObject.Find("Statistics").GetComponent<Text>();

        double numHits = Conductor.instance.notesHit;
        double numNotes = numHits + Conductor.instance.notesMissed;
        double percentage = Math.Round(numHits / numNotes, 2) * 100;

        statistics.text = "Notes hit: " + numHits + " / " + numNotes + "\nPercent hit: " + percentage + "%" + "\nRetries: " + MapData.instance.numRetries;
    }
}
