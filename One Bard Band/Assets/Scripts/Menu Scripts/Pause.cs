﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            togglePause();
        }
    }

    public void MainMenu()
    {
        togglePause();
        SceneManager.LoadScene("MainMenu");
    }
    public void Restart()
    {
        string sceneName = MapData.instance.levelName;
        MapData.instance.Unload();
        togglePause();
        SceneManager.LoadScene(sceneName);
    }

    public void togglePause()
    {

        MapData.instance.songs.TogglePauseMusic();

        try
        {
            Conductor.instance.TogglePause();
        } catch (NullReferenceException)
        {
            UnityEngine.Debug.Log("Paused in overworld");
        } catch (MissingReferenceException)
        {
            UnityEngine.Debug.Log("Paused in overworld");
        }

        if (Time.timeScale == 0f)
        {
            Time.timeScale = 1f;
            GameObject.Find("Pause(Clone)").SetActive(false);
        }
        else
        {
            Time.timeScale = 0f;
            Instantiate(Resources.Load("Menu/Pause"));
        }
    }
}