﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public void Retry()
    {
        //MapData.instance.Unload();
        MapData.instance.numRetries += 1;
        MapData.instance.songs.inGameOver = false;
        BattleBard.curHealth = BattleBard.maxHealth;
        SceneManager.LoadScene("BattleScene");
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    void Awake()
    {
        MapData.instance.songs.StopMusic();
    }

}
