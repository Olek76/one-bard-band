﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame(string scene)
    {
        SceneManager.LoadSceneAsync(scene);
    }

    public void ChangeScreen(string menuObj)
    {
        GameObject go = Instantiate(Resources.Load("Menu/" + menuObj), new Vector3(-1, 0, 0), Quaternion.identity) as GameObject;
        go.transform.SetParent(GameObject.Find("Canvas").transform, false);
        this.transform.parent.gameObject.SetActive(false);
    }
    

    public void options()
    {

    }

    public void ExitGame()
    {
        Application.Quit();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (MapData.instance)
        {
            MapData.instance.Unload();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
