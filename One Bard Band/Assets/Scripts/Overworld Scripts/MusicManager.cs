﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour
{

    public SongData overworldSong;
    public SongData battleSong;
    public SongData bossSong;
    public SongData activeSong;


    // syncing flags
    public bool inBattle;
    public bool inBoss;
    public bool enemyCanPlay;
    public bool hasStarted;
    public bool enemySoundsStopped;
    public bool inGameOver;
    public bool paused;

    ///////////////////////Ported vars from NoteScroller
 
    //Current phrase position, in seconds
    public double phrasePosition;

    //Current phrase position for hearing, in beats
    public double hearingPositionInBeats;

    //Current phrase position for loading prefabs, in beats
    public double loadingPositionInBeats;

    //Modifier to adjust how many measures ahead notes will appear
    //public int loadMeasureMod;

    //How many seconds have passed since the song started
    public double dspSongTime;

    //The offset to the first beat of the song in seconds
    public double firstBeatOffset;

    //The offset of all previous phrases summed up
    public double phraseOffset;

    //The offset of total time spent paused
    public double totalPauseOffset;

    //The offset of the most recent/current time paused
    public double currPauseOffset;

    //The dspTime of the last pause (used to calc totalPauseOffset) and most recent 
    public double currPauseDsp;

    /////////////////////////End ported vars

    public double SecToBeat(double seconds)
    {
        return seconds * (activeSong.tempo / 60);
    }

    public double BeatToSec(double beats)
    {
        return beats * (60 / activeSong.tempo);
    }


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        //Initialize audio data
        if (!hasStarted && !inGameOver)
        {
            dspSongTime =  AudioSettings.dspTime;

            Debug.Log("Starting Overworld Music (should only appear once per level attempt!!)");
            overworldSong.audioSrc.mute = false;
            overworldSong.audioSrc.Play();
            //battleSong.audioSrc.mute = true;
            battleSong.audioSrc.Play();
            
            hasStarted = true;
        } 

        //After audio data has been initialized
        else if (!inGameOver)
        {

            // If paused, increment currPauseOffset to reflect the additional time taken
            if (paused) currPauseOffset =  AudioSettings.dspTime - currPauseDsp;


            // Always updates phrase Position in seconds & beats
            phrasePosition = (AudioSettings.dspTime - dspSongTime - firstBeatOffset - phraseOffset - totalPauseOffset - currPauseOffset);
            hearingPositionInBeats = SecToBeat(phrasePosition);

            if (inBattle)
            {
                try
                {

                    //if the first phrase on which the player entered BattleScene has ended, i.e. then enemyCanPlay == true
                    if (enemyCanPlay)
                    {
                        loadingPositionInBeats = hearingPositionInBeats - (Conductor.instance.noteScroll.activePhrase.length - (activeSong.beatsPerMeasure * activeSong.loadMeasureMod));
                        Conductor.instance.noteScroll.LoadNote(hearingPositionInBeats, loadingPositionInBeats);

                        if (!enemySoundsStopped && loadingPositionInBeats >= activeSong.beatsPerMeasure * activeSong.loadMeasureMod)
                        {
                            Conductor.instance.StopAllEnemySounds();
                            enemySoundsStopped = true;
                        }

                        if (loadingPositionInBeats < 0)
                        {
                            Conductor.instance.noteScroll.SetNowHealing(false);
                        }

                        if (hearingPositionInBeats >= 2 * Conductor.instance.noteScroll.phrasesHeard.Peek().length)
                        {
                            Conductor.instance.noteScroll.LoadPhraseFromStage();
                            phraseOffset += 2 * BeatToSec(Conductor.instance.noteScroll.phrasesHeard.Dequeue().length);
                            enemySoundsStopped = false;
                        }
                    }
                    //if the first phrase on which the player entered BattleScene has NOT ended
                    else
                    {
                        loadingPositionInBeats = hearingPositionInBeats;
                        if (hearingPositionInBeats >= 2 * activeSong.beatsPerMeasure)
                        {
                            //Conductor.instance.noteScroll.LoadPhraseFromStage();
                            phraseOffset += 2 * BeatToSec(activeSong.beatsPerMeasure);
                            enemyCanPlay = true;

                            if (inBoss)
                            {
                                overworldSong.audioSrc.mute = true;
                                battleSong.audioSrc.mute = true;
                                bossSong.audioSrc.mute = false;
                                bossSong.audioSrc.Play();
                                activeSong = bossSong;
                            }

                        }

                    }

                }
                catch (NullReferenceException)
                {
                    Debug.Log("Conductor's Note Scroll doesn't exist!");
                }


            }
            //if in overworld:
            else
            {
                loadingPositionInBeats = hearingPositionInBeats;
                if (hearingPositionInBeats >= 2 * activeSong.beatsPerMeasure)
                {
                    phraseOffset +=  (2 * BeatToSec(activeSong.beatsPerMeasure));
                }
            }
        }


    }

    void Awake()
    {
        overworldSong = GameObject.FindWithTag("OverworldMusic").GetComponent<SongData>();
        battleSong = GameObject.FindWithTag("BattleMusic").GetComponent<SongData>();
        bossSong = GameObject.FindWithTag("BossMusic").GetComponent<SongData>();

        battleSong.audioSrc.mute = true;
        overworldSong.audioSrc.mute = true;

        // init sync flags
        inBattle = false;
        enemyCanPlay = false;
        hasStarted = false;
        enemySoundsStopped = false;
        inGameOver = false;
        inBoss = false;
        paused = false;

        activeSong = overworldSong;

        //loadMeasureMod = 1;
        dspSongTime = -1;
        firstBeatOffset = 0;
        phraseOffset = 0;
        totalPauseOffset = 0;
        currPauseOffset = 0;
        currPauseDsp = 0;

    }

    public void Transition()
    {

        inBattle = !inBattle;
        battleSong.audioSrc.mute = !battleSong.audioSrc.mute;
        overworldSong.audioSrc.mute = !overworldSong.audioSrc.mute;

        //bool isBoss = false;
        if (inBattle)
        {
            activeSong = battleSong;
        } 
        else
        {
            activeSong = overworldSong;
            enemyCanPlay = false;
            enemySoundsStopped = false;
            
        }

    }

    //used when combat is started from gameover screen
    public void StartBattleMusic()
    {
        inBattle = true;
        overworldSong.audioSrc.mute = true;
        activeSong = battleSong;
        battleSong.audioSrc.mute = false;
    }


    public void StartBossMusic()
    {
        bossSong.audioSrc.Play();
        activeSong = bossSong;

        inBattle = true;
        enemyCanPlay = true;

        overworldSong.audioSrc.mute = true;
        battleSong.audioSrc.mute = true;
        bossSong.audioSrc.mute = false;

        phraseOffset += phrasePosition;
        try
        {
            Conductor.instance.noteScroll.LoadPhraseFromStage();
        } catch (NullReferenceException)
        {

        }

    }

    public void PrepBossMusic()
    {
        inBoss = true;
    }

    public void StopMusic()
    {
        overworldSong.audioSrc.mute = true;
        battleSong.audioSrc.mute = true;
        bossSong.audioSrc.mute = true;
    }



    //Called whenever the pause menu is toggled
    public void TogglePauseMusic()
    {

        paused = !paused;

        if (paused)
        {
            currPauseDsp = AudioSettings.dspTime;

            bossSong.audioSrc.Pause();
            overworldSong.audioSrc.Pause();
            battleSong.audioSrc.Pause();

        }
        else
        {
            totalPauseOffset += currPauseOffset;
            currPauseOffset = 0f;

            bossSong.audioSrc.UnPause();
            overworldSong.audioSrc.UnPause();
            battleSong.audioSrc.UnPause();

        }

    }

    public void SetGameOverValues()
    {
        inBattle = false;
        enemyCanPlay = false;
        hasStarted = false;
        enemySoundsStopped = false;
        inGameOver = true;
        inBoss = false;

        phraseOffset += phrasePosition;
        phrasePosition = (AudioSettings.dspTime - dspSongTime - firstBeatOffset - phraseOffset);
        hearingPositionInBeats = SecToBeat(phrasePosition);
        phraseOffset = 0f;

    }



}
