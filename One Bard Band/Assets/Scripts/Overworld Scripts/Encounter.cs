﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Encounter : MonoBehaviour
{
    public OverworldManager manager;
    public int id; //index of encounter in Encounters .txt file

    //triggers combat
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log(manager);
        OverworldBard bard = manager.GetComponentInChildren<OverworldBard>();
        bard.canMove = false;

        GetComponent<EdgeCollider2D>().enabled = false;

        StartCoroutine(BeginEncounter(bard));
    }

    //clear encounter
    public void Clear()
    {
        Destroy(gameObject);
    }

    void Start()
    {
        if (MapData.instance.lineups[id].IsCleared())
        {
            Clear();
        }
    }

    IEnumerator BeginEncounter(OverworldBard bard)
    {
        // get sprites and their original positions
        NPC[] sprites = GetComponentsInChildren<NPC>();
        Vector3[] originalPositions = new Vector3[sprites.Length];

        for (int i = 0; i < sprites.Length; i++)
        {
            originalPositions[i] = sprites[i].transform.position;
        }

        // move sprites towards player sprite
        float t = 0;
        bool arrived = false;

        while (!arrived)
        {
            // lerp sprite position between position and player position
            t += Time.deltaTime / 0.5f;
            for (int i = 0; i < sprites.Length; i++)
            {
                sprites[i].transform.position = Vector3.Lerp(originalPositions[i], bard.transform.position, t);
                
                if (sprites[i].GetComponent<NPC>().reachedPlayer)
                    arrived = true;
            }

            //arrived = (t >= 1) ? true : false;

            yield return null;
        }
        
        MapData.instance.StoreData(id, GameObject.Find("OverworldBard").transform.position);
        manager.LoadNewCombat(MapData.instance.GetEnemies());
    }
}
