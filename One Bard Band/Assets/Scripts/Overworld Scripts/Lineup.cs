﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//static class that stores encounter info
public class Lineup
{
    public List<string> enemies;
    private bool isCleared;

    public Lineup()
    {
        enemies = new List<string> { };
        isCleared = false;
    }

    public Lineup(string[] enemyArr)
    {
        enemies = new List<string> { };
        foreach (string enemy in enemyArr)
        {
            enemies.Add(enemy);
        }

        isCleared = false;
    }

    public void SetCleared()
    {
        isCleared = true;
    }
    public bool IsCleared()
    {
        return isCleared;
    }
}