﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OverworldManager : MonoBehaviour
{
    public static OverworldManager instance;
    public GameObject bard;

    void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        bard.transform.position = MapData.instance.GetPosition();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadNewCombat(List<string> enemies)
    {
        StartCoroutine(LoadNewCombatPrivate(enemies));
        //MapData.instance.songs.Transition();

    }

    private IEnumerator LoadNewCombatPrivate(List<string> enemies)
    {
        SceneManager.LoadSceneAsync("BattleScene");
        MapData.instance.songs.Transition();
        yield return 0;
    }
}
