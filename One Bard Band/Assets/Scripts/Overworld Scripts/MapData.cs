﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//acts as "static class" storing map data
public class MapData : MonoBehaviour
{
    public static MapData instance;

    public string levelName; //name of specific level data
    public List<Lineup> lineups;
    public int curEncounter = 0; // id of current encounter
    public Vector3 pos; //bard location
    public MusicManager songs;
    public int numRetries = 0;  

    public List<Lineup> GetLineups()
    {
        return lineups;
    }
    public List<string> GetEnemies()
    {
        return lineups[curEncounter].enemies;
    }
    public Vector3 GetPosition()
    {
        return pos;
    }
    public void SetPosition(Vector3 position)
    {
        pos = position;
    }
    //this sets the index for the encounters list
    public void SetEncounter(int id)
    {
        curEncounter = id;
    }

    public void StoreData(int encounter, Vector3 position)
    {
        SetPosition(position);
        SetEncounter(encounter);
    }

    public virtual void ReloadMap() {}

    //unload instance
    public void Unload()
    {
        Destroy(gameObject);
    }


}
