﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SirenCave : MapData
{
    //restore data after combat, reloads map, marks current encounter as cleared
    public override void ReloadMap()
    {
        songs.Transition();
        lineups[curEncounter].SetCleared();
        StartCoroutine(LoadMap());
    }

    private IEnumerator LoadMap()
    {
        
        SceneManager.LoadScene("SirenCave");
        //SceneManager.SetActiveScene(SceneManager.GetSceneByName("GoblinCamp"));
        //SceneManager.UnloadScene("BattleScene");
        yield return new WaitForSeconds(1f);
    }

    void Awake()
    {
        if (MapData.instance != null)
        {
            Unload();
        }
        else
        {
            levelName = "SirenCave";
            //Debug.Log("MapData instance was null (SHOULD ONLY BE CALLED ONCE!!!");
            instance = this;
            pos = new Vector3(-26.4f, -11.0f, 0.0f);

            lineups = new List<Lineup>
            {
                new Lineup( new string[]{"Enemies/SirenA"} ),
                new Lineup( new string[]{"Enemies/SirenA", "Enemies/CaveGoblinA"} ),
                new Lineup( new string[]{ "Enemies/CaveGoblinB", "Enemies/SirenA", "Enemies/CaveGoblinA" } ) ,
                new Lineup( new string[]{ "Enemies/SirenA", "Enemies/SirenA", "Enemies/CaveGoblinC" } ),
                new Lineup( new string[]{ "Enemies/SirenQueen"} )
            };
            BattleBard.curHealth = BattleBard.maxHealth;
            songs = gameObject.AddComponent<MusicManager>();

        }

    }
    void Start()
    {
        Application.targetFrameRate = 30;
    }
    void Update()
    {
        DontDestroyOnLoad(gameObject);
    }
}