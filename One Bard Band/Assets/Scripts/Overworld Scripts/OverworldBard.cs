﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverworldBard : MonoBehaviour
{

    // bard's overworld movement speed
    public float speed = 5f;

    public bool canMove = true;

    public Animator animator;

    //SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
       // spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
            MoveBard();
    }

    void MoveBard()
    {
        // get input from WASD
        bool moveUp = Input.GetKey(KeyCode.W);
        bool moveLeft = Input.GetKey(KeyCode.A);
        bool moveDown = Input.GetKey(KeyCode.S);
        bool moveRight = Input.GetKey(KeyCode.D);

        // initiailze animator booleans
        bool animW = false;
        bool animS = false;
        bool animD = false;
        bool animA = false;


        float dY = 0;
        float dX = 0;

        // get vertical movement
        if (moveUp && !moveDown)
        {
            dY += speed;
            animW = true;
        }
        else if (moveDown && !moveUp)
        {
            dY -= speed;
            animS = true;
        }

        // get horizontal movement
        if (moveRight && !moveLeft)
        {
            dX += speed;
            animD = true;
        }
        else if (moveLeft && !moveRight)
        {
            dX -= speed;
            animA = true;
        }
        
        // tell animator what to do
        animator.SetBool("W", animW);
        animator.SetBool("S", animS);
        animator.SetBool("D", animD);
        animator.SetBool("A", animA);

        // move bard
        Vector3 movementVector = new Vector3(dX, dY, 0);
        transform.Translate(movementVector.normalized * speed * Time.deltaTime);
    }
}