﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoblinCamp : MapData
{
    //restore data after combat, reloads map, marks current encounter as cleared
    public override void ReloadMap()
    {
        songs.Transition();
        lineups[curEncounter].SetCleared();
        StartCoroutine(LoadMap());
    }

    private IEnumerator LoadMap()
    {
        
        SceneManager.LoadScene("GoblinCamp");
        //SceneManager.SetActiveScene(SceneManager.GetSceneByName("GoblinCamp"));
        //SceneManager.UnloadScene("BattleScene");
        yield return new WaitForSeconds(1f);
    }

    void Awake()
    {
        if (MapData.instance != null)
        {
            Unload();
        }
        else
        {
            levelName = "GoblinCamp";
            Debug.Log("MapData instance was null (SHOULD ONLY BE CALLED ONCE!!!");
            instance = this;
            pos = new Vector3(-22.0f, 11.0f, 0.0f);

            lineups = new List<Lineup>
            {
                new Lineup( new string[]{"Enemies/GoblinA"} ),
                new Lineup( new string[]{"Enemies/GoblinC"} ),
                new Lineup( new string[]{ "Enemies/GoblinA", "Enemies/GoblinB" } ) ,
                new Lineup( new string[]{ "Enemies/GoblinA", "Enemies/GoblinB", "Enemies/GoblinC" } ),
                new Lineup( new string[]{ "Enemies/GoblinChief"} )
            };
            BattleBard.curHealth = BattleBard.maxHealth;
            songs = gameObject.AddComponent<MusicManager>();

        }

    }
    void Start()
    {
        Application.targetFrameRate = 30;
    }
    void Update()
    {
        DontDestroyOnLoad(gameObject);
    }
}