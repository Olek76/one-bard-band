﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SirenBossTrigger : MonoBehaviour
{
    public bool waitActive = false; 
    public OverworldManager manager;

    IEnumerator End()
    {
        waitActive = true;
        yield return new WaitForSeconds(1);
        Instantiate(Resources.Load("Menu/Siren End"));
        waitActive = false;
    }
    //triggers combat
    private void OnTriggerEnter2D(Collider2D other)
    {
        OverworldBard bard = manager.GetComponentInChildren<OverworldBard>();
        bard.canMove = false;
        StartCoroutine(End());
        GetComponent<EdgeCollider2D>().enabled = false;
    }
}
