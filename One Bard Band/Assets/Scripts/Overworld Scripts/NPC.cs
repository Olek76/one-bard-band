﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class NPC : MonoBehaviour
{
    public bool reachedPlayer = false;

    private void OnTriggerEnter2D(Collider2D other)
    {
        // check if other object is player
        if (other.CompareTag("Player"))
        {
            //Debug.Log("Reached player");
            reachedPlayer = true;
            GetComponent<BoxCollider2D>().enabled = false;
        }
    }
}
